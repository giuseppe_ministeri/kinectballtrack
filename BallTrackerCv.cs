﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using Microsoft.Kinect;
using System.Windows.Media.Imaging;
using OpenCvSharp.Extensions;
using System.Windows.Media;
using System.Net.Sockets;

namespace Microsoft.Samples.Kinect.ColorBasics
{
    class BallTrackerCv
    {
        public ColorObject ballColor = new ColorObject("ballColor");
        private Mat rgbMat;
        private Mat hsvMat;
        private Mat hsvMatBlur;
        private Mat bSMask;
        private Mat blurRgbMat;
        private Mat thresholdMat;

        public Size erosion;
        public Size dilatation;

        public double width = 1920;
        public double height = 1080;

        private double resW, resH;

        BackgroundSubtractorMOG2 bSMOG2;

        public unsafe ushort[] depthFrameData;
        public ColorFrame colorFrame;

        public Scalar spacePoint;


        public WriteableBitmap ColorBitmap
        {
            private get
            {
                return _colorBitmap;
            }

            set
            {
                _colorBitmap = value;

                this.width = _colorBitmap.Width;
                this.height = _colorBitmap.Height;

                rgbMat = _colorBitmap.ToMat();


                Cv2.Resize(rgbMat, rgbMat, new Size(resW, resH), resW, resH, InterpolationFlags.Linear);
                Cv2.Blur(rgbMat, blurRgbMat, new Size(11, 11));
                Cv2.CvtColor(blurRgbMat, hsvMat, ColorConversionCodes.RGB2HSV);

                Cv2.InRange(hsvMat, ballColor.getHSVmin(), ballColor.getHSVmax(), thresholdMat);



                //bSMOG2.Apply(hsvMat, bSMask);
                //thresholdMat = thresholdMat.Mul(bSMask);

                morphOps(thresholdMat);


                Point center2D = trackFilteredObject(ballColor, thresholdMat, hsvMat, rgbMat);


                depthMapper(center2D);


                if (center2D.X != -1 && center2D.Y != 1)
                {
                    if (!Double.IsInfinity(spacePoint.Val0) && !Double.IsInfinity(spacePoint.Val1) && !Double.IsInfinity(spacePoint.Val2))
                    {
                       //System.Diagnostics.Trace.WriteLine("sending "+ spacePoint.Val0 + ";" + spacePoint.Val1 + ";" + spacePoint.Val2);

                        byte[] byteData = Encoding.ASCII.GetBytes("point;"+spacePoint.Val0 + ";" + spacePoint.Val1 + ";" + spacePoint.Val2);
                        sent = true;
                        SendUdp(8050, "127.0.0.1", 8051, byteData);
                    }
                }

                center2D.Y = rgbMat.Height - center2D.Y;
                rgbMat = rgbMat.Flip(FlipMode.X);
                drawBall(rgbMat, center2D);
                rgbMat = rgbMat.Flip(FlipMode.X);


                Cv2.ImShow("Kin", rgbMat);
               // Cv2.ImShow("KinThresh", thresholdMat);

                sent = false;
            }
        }
        private WriteableBitmap _colorBitmap;
        private readonly float MIN_OBJECT_AREA = 0;
        private readonly int factorResize = 3;
        private bool sent = false;

        public Mat FrameRgb { get; set; }
        public int currentRadius { get; private set; }
        public CoordinateMapper CoordinateMapper { get; internal set; }

        public BallTrackerCv()
        {
 //           erosion = new Size(1, 1);
 //           dilatation = new Size(10, 10);
            bSMOG2 = BackgroundSubtractorMOG2.Create();
            hsvMat = new Mat();
            hsvMatBlur = new Mat();
            thresholdMat = new Mat();
            bSMask = new Mat();
            blurRgbMat = new Mat();
            resW = this.width / factorResize;
            resH = this.height / factorResize;
//            ballColor.setColor(new Scalar(95, 40, 0));
//            ballColor.setColor(new Scalar(70, 170, 0));

        }

        public void morphOps(Mat thresh)
        {
            //create structuring element that will be used to "dilate" and "erode" image.
            //the element chosen here is a 3px by 3px rectangle
            Mat erodeElement = Cv2.GetStructuringElement(MorphShapes.Ellipse, erosion);
            //dilate with larger element so make sure object is nicely visible
            Mat dilateElement = Cv2.GetStructuringElement(MorphShapes.Ellipse, dilatation);

            Cv2.Erode(thresh, thresh, erodeElement);
            Cv2.Erode(thresh, thresh, erodeElement);

            Cv2.Dilate(thresh, thresh, dilateElement);
            Cv2.Dilate(thresh, thresh, dilateElement);
        }

        Point trackFilteredObject(ColorObject theColorObject, Mat threshold, Mat HSV, Mat cameraFeed)
        {
            //these two vectors needed for output of findContours
            Point[][] contours;
            HierarchyIndex[] hierarchy;
            Point2f center = new Point2f();
            float rad = 0;


            //find contours of filtered image using openCV findContours function
            Cv2.FindContours(threshold, out contours, out hierarchy, RetrievalModes.CComp, ContourApproximationModes.ApproxSimple);
            //use moments method to find our filtered object

            if (contours.Length > 0)
            {
                int maxIndex = -1; double maxArea = -1;
                for (int index = 0; index < contours.Length; index++)
                {
                    var t = Cv2.ContourArea(contours[index]);
                    if (t > maxArea)
                    {
                        maxIndex = index;
                        maxArea = t;
                    }
                }


                Cv2.MinEnclosingCircle(contours[maxIndex], out center, out rad);
                var mm = Cv2.Moments(contours[maxIndex]);
                Point cv = new Point((float)(mm.M10 / mm.M00), (float)(mm.M01 / mm.M00));
                currentRadius = (int)rad;
                if (rad > MIN_OBJECT_AREA)
                {
                    return cv;
                }
                else
                {
                    return new Point(-1, -1);
                }

            }

            return new Point(-1, -1);
        }


        void drawBall(Mat cameraFeed, Point cv)
        {
            string s = cv.X + " " + cv.Y;
            //BGR
            Scalar color = new Scalar(0, 0, 215);
            if (sent)
            {
                color = new Scalar(0, 215, 0);
                string x = spacePoint.Val0.ToString();
                if(x.Length>5)x=x.Substring(0, 5);
                string y = spacePoint.Val1.ToString();
                if(y.Length>5)y=y.Substring(0, 5);
                string z = spacePoint.Val2.ToString();
                if(z.Length>5)z=z.Substring(0, 5);
                s = x+";"+y+";"+z;
            }
            //Cv2.drawContours(cameraFeed, contours, maxIndex, ballColor.getColor(), 10, 8, hierarchy, int.MaxValue, new Point());
            Point textPoint = new Point(cv.X+currentRadius,cv.Y+currentRadius);
            Cv2.Circle(cameraFeed, cv, currentRadius, color, 8 / factorResize);
            Cv2.PutText(cameraFeed, s, textPoint, HersheyFonts.HersheyPlain, 1, color, 2, LineTypes.AntiAlias, true);
            
        }
        unsafe void depthMapper(Point center2D)
        {
            //depth calc
            if (center2D.X != -1 && center2D.Y != -1)
            {
//                Point ballVect = new Point((int)center2D.X, (rgbMat.Height - (int)center2D.Y));
                Point ballVect = new Point((int)center2D.X, (int)center2D.Y);
                ballVect = ballVect * factorResize;

                CameraSpacePoint[] cameraSpacePoints = new CameraSpacePoint[(int)(this.width * this.height)];
                CoordinateMapper.MapColorFrameToCameraSpace(depthFrameData, cameraSpacePoints);

                int t = ballVect.Y * (int)this.width + ballVect.X;
                CameraSpacePoint mySweetCameraPoint = cameraSpacePoints[t];
                spacePoint = new Scalar(mySweetCameraPoint.X, mySweetCameraPoint.Y, mySweetCameraPoint.Z);

                /*
                for (int i = 0; i <= currentRadius ; i++)
                {
                    int v = (int)this.width * ballVect.Y + ballVect.X;

                    int t = (i + ballVect.Y) * (int)this.width + (i + ballVect.X);

                    //int t = ((int)this.width + 1) * i;
                    //int j = t * (i % 2 == 0 ? 1 : -1);
                    CameraSpacePoint mySweetCameraPoint = cameraSpacePoints[t];
                    spacePoint = new Scalar(mySweetCameraPoint.X, mySweetCameraPoint.Y, mySweetCameraPoint.Z);
                    if (!Double.IsInfinity(spacePoint.Val0)) break;
                }
                */
            }
        }
        public void SendFloorPlaneClipping(Vector4 floor)
        {
                byte[] byteData = Encoding.ASCII.GetBytes("floor;" + floor.X + ";" + floor.Y + ";" + floor.Z + ";" + floor.W);
                SendUdp(8050, "127.0.0.1", 8052, byteData);
            
        }

        static void SendUdp(int srcPort, string dstIp, int dstPort, byte[] data)
        {
            using (UdpClient c = new UdpClient(srcPort))
            {
                c.Send(data, data.Length, dstIp, dstPort);
                c.Close();
            }


        }

        internal void setErosion(double value)
        {
            erosion.Width = erosion.Height = (int)value;
        }

        internal void setDilatation(double value)
        {
            dilatation.Width = dilatation.Height = (int)value;
        }
    }
}
