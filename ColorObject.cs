﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace Microsoft.Samples.Kinect.ColorBasics
{
    
    public class ColorObject
    {
        int xPos, yPos;
        string type;
        Scalar HSVmin, HSVmax;
        Scalar color;
        public float Hthreshold { get; set; }
        public float Sthreshold { get; set; }
        public float Vthreshold { get; set; }


        public ColorObject()
        {
            //set values for default constructor
            setType("Object");
            setColor(new Scalar(0, 0, 0));
        }

        public ColorObject(string name)
        {
            setType(name);

            if (name == "blue")
            {

                //TODO: use "calibration mode" to find HSV minCanvasGroup
                //and HSV max values

                setHSVmin(new Scalar(0, 0, 0));
                setHSVmax(new Scalar(256, 256, 256));
                //           setHSVmin(new Scalar(92, 0, 0));
                //            setHSVmax(new Scalar(124, 256, 256));

                //BGR value for Green:
                setColor(new Scalar(0, 0, 255));

            }
            if (name == "green")
            {

                //TODO: use "calibration mode" to find HSV minCanvasGroup
                //and HSV max values

                setHSVmin(new Scalar(34, 50, 50));
                setHSVmax(new Scalar(80, 220, 200));

                //BGR value for Yellow:
                setColor(new Scalar(0, 255, 0));

            }
            if (name == "yellow")
            {

                //TODO: use "calibration mode" to find HSV minCanvasGroup
                //and HSV max values

                setHSVmin(new Scalar(20, 124, 123));
                setHSVmax(new Scalar(30, 256, 256));

                //BGR value for Red:
                setColor(new Scalar(255, 255, 0));

            }
            if (name == "red")
            {

                //TODO: use "calibration mode" to find HSV minCanvasGroup
                //and HSV max values

                setHSVmin(new Scalar(0, 200, 0));
                setHSVmax(new Scalar(19, 255, 255));

                //BGR value for Red:
                setColor(new Scalar(255, 0, 0));

            }

            if (name == "ballColor")
            {

                //TODO: use "calibration mode" to find HSV minCanvasGroup
                //and HSV max values

                setHSVmin(new Scalar(0, 200, 0));
                setHSVmax(new Scalar(19, 255, 255));

                //BGR value for Red:
                setColor(new Scalar(105, 70, 0));

                setHthreshold(23.5f);
                setSthreshold(104.8f);
                setVthreshold(154.9f);
            }
        }

        public int getXPos()
        {
            return xPos;
        }

        public void setXPos(int x)
        {
            xPos = x;
        }

        public int getYPos()
        {
            return yPos;
        }

        public void setYPos(int y)
        {
            yPos = y;
        }

        public Scalar getHSVmin()
        {
            return HSVmin;
        }

        public Scalar getHSVmax()
        {
            return HSVmax;
        }

        public void setHSVmin(Scalar min)
        {
            HSVmin = min;
        }

        public void setHSVmax(Scalar max)
        {
            HSVmax = max;
        }

        public string getType()
        {
            return type;
        }

        public void setType(string t)
        {
            type = t;
        }

        public Scalar getColor()
        {
        //    System.Diagnostics.Trace.WriteLine(color);
            return color;
        }

        public void setHthreshold(float hT)
        {
            Hthreshold = hT;
            setColor(getColor());
        }

        public void setSthreshold(float sT)
        {
            Sthreshold = sT;
            setColor(getColor());
        }

        public void setVthreshold(float vT)
        {
            Vthreshold = vT;
            setColor(getColor());
        }

        public void setColor(Scalar c)
        {

            color = c;

            Mat hsv = new Mat();
            Mat rgb = new Mat(1, 1,MatType.CV_8UC3, c);

            Cv2.CvtColor(rgb, hsv, ColorConversionCodes.RGB2HSV);
            Scalar hsv_color = new Scalar((int)rgb.At<Vec3b>(0, 0)[0], (int)rgb.At<Vec3b>(0, 0)[1], (int)rgb.At<Vec3b>(0, 0)[2]);

            float h, s, v;
            h = (float)hsv_color.Val0;
            s = (float)hsv_color.Val1;
            v = (float)hsv_color.Val2;

            //setHSVmin(new Scalar(hsv_color.Val0 - 10, 100, 100));
            //setHSVmax(new Scalar(hsv_color.Val0 + 10, 255, 255));
         // 30 190 220 1 5
         //23.5 104.8
            setHSVmin(new Scalar(h - Hthreshold, s - Sthreshold, v - Vthreshold));
            setHSVmax(new Scalar(h + Hthreshold, s + Sthreshold, v + Vthreshold));
        }
    }
}